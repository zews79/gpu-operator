FROM docker.io/golang:1.10
WORKDIR /go/src/github.com/zvonkok/special-resource-operator
COPY . .
RUN make build

FROM quay.io/openshift/origin-pod:4.1
COPY --from=0 /go/src/github.com/zvonkok/special-resource-operator/special-resource-operator /usr/bin/

RUN mkdir -p /opt/sro
COPY assets/ /opt/sro

RUN useradd special-resource-operator
USER special-resource-operator
ENTRYPOINT ["/usr/bin/special-resource-operator"]
LABEL io.k8s.display-name="OpenShift Special Resource Operator" \
      io.k8s.description="This is a component of OpenShift and manages special resources." \
      io.openshift.release.operator=true
